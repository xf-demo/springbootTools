# springBoot 工具项目
本项目后台基于springboot,前端使用layui,可用于搭建后台管理系统

## 启动 
1. 修改数据库连接
2. 启动:`mvn spring-boot:run`
3. 初始登录 admin/admin 

## 技术
- 后端springBoot
- 前端layui
- ORM技术:JPA、Mybatis、通用Mapper、PageHelper分页
- 数据库版本管理：liquibase
- 安全性采用springSecurity，spring-session 用户session存入redis(可配置成存在tomcat容器中)

## 已实现功能
- 代码自动生成器
- quartz定时任务,可通过后台页面进行管理

## 预览
![](https://i.loli.net/2018/01/23/5a66f544cd7c6.jpg)
![](https://i.loli.net/2018/01/23/5a670bdede265.jpg)

## TODO
- 登录密码加盐
