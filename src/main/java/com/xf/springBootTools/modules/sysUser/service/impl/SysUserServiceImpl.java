package com.xf.springBootTools.modules.sysUser.service.impl;

import com.xf.springBootTools.common.service.BaseService;
import com.xf.springBootTools.common.service.impl.BaseServiceImpl;
import com.xf.springBootTools.modules.sysUser.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xf.springBootTools.modules.sysUser.dto.SysUser;
import com.xf.springBootTools.modules.sysUser.service.SysUserService;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysUserServiceImpl extends BaseServiceImpl<SysUser> implements SysUserService{
	@Autowired
	private SysUserMapper sysUserMapper;

	@Override
	public List<SysUser> findAllUsers() {
		List<SysUser> users = sysUserMapper.findAllUsers();
		return users;
	}
}