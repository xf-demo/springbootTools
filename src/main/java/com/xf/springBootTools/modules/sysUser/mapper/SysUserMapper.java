package com.xf.springBootTools.modules.sysUser.mapper;

import com.xf.springBootTools.config.mybatis.MyMapper;
import com.xf.springBootTools.modules.sysUser.dto.SysUser;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysUserMapper extends MyMapper<SysUser> {

	List<SysUser> findAllUsers();
}