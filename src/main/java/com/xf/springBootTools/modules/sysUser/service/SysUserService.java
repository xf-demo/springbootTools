package com.xf.springBootTools.modules.sysUser.service;

import com.xf.springBootTools.modules.sysUser.dto.SysUser;
import com.xf.springBootTools.common.service.BaseService;

import java.util.List;

public interface SysUserService extends BaseService<SysUser>{

	List<SysUser> findAllUsers();
}