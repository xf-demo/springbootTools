package com.xf.springBootTools.modules.sysUser.controllers;

import com.xf.springBootTools.modules.sysUser.mapper.SysUserMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import com.xf.springBootTools.common.pojo.core.ResponseData;
import com.xf.springBootTools.modules.sysUser.dto.SysUser;
import com.xf.springBootTools.modules.sysUser.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

import org.springframework.validation.BindingResult;

import java.util.List;

@Controller
@RequestMapping("/user")
public class SysUserController {
	@Autowired
	private SysUserService sysUserService;

	@RequestMapping(value = "/index")
	public String index() {
		return "user/userList";
	}

	@RequestMapping(value = "/edit")
	public String edit() {
		return "user/userEdit";
	}

	@RequestMapping(value = "/query")
	@ResponseBody
	public ResponseData query() {
		List<SysUser> users = sysUserService.findAllUsers();
		return new ResponseData(users);
	}

	@RequestMapping(value = "/create")
	@ResponseBody
	public ResponseData create(HttpServletRequest request, @RequestBody SysUser dto) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext()
				.getAuthentication()
				.getPrincipal();
		return new ResponseData();
	}



	@RequestMapping(value = "/remove")
	@ResponseBody
	public ResponseData delete(HttpServletRequest request, @RequestBody List<SysUser> dto) {
		return new ResponseData();
	}
}