package com.xf.springBootTools.modules.quartz.mapper.repository;

import com.xf.springBootTools.modules.quartz.dto.QrtzTriggers;
import com.xf.springBootTools.modules.quartz.dto.QrtzTriggersPk;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author zhuweifeng
 * @date 2018/1/29
 */
@Repository
public interface JobTriggersRepository extends JpaRepository<QrtzTriggers, QrtzTriggersPk> {
	QrtzTriggers findQrtzTriggersById(QrtzTriggersPk qrtzTriggersPk);
}

