package com.xf.springBootTools.modules.quartz.service.impl;

import com.github.pagehelper.PageHelper;
import com.xf.springBootTools.common.service.impl.BaseServiceImpl;
import com.xf.springBootTools.modules.quartz.constant.QuartzConstant;
import com.xf.springBootTools.modules.quartz.dto.JobAndTrigger;
import com.xf.springBootTools.modules.quartz.mapper.JobAndTriggerMapper;
import com.xf.springBootTools.modules.quartz.service.JobAndTriggerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author xiaofeng
 * @date 2018/1/29
 */
@Service
public class JobAndTriggerImpl extends BaseServiceImpl<JobAndTrigger> implements JobAndTriggerService {

	@Autowired
	private JobAndTriggerMapper jobAndTriggerMapper;

	@Override
	public List<JobAndTrigger> getJobAndTriggerDetails(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<JobAndTrigger> list = jobAndTriggerMapper.getJobAndTriggerDetails();
		return list;
	}

	@Override
	public JobAndTrigger findJobDetail(String optionType, String jobName, String jobGroup) {
		if(QuartzConstant.OPT_TYPE_CREATE.equals(optionType)){
			JobAndTrigger jobAndTrigger = new JobAndTrigger();
			jobAndTrigger.setJobGroup(QuartzConstant.DEFAULT_GROUP);
			jobAndTrigger.setStartTime(System.currentTimeMillis());
			jobAndTrigger.setEndTime(4070880000000L);
			return jobAndTrigger;
		}
		return jobAndTriggerMapper.findJobDetail(QuartzConstant.DEFAULT_SCHEDULER_FACTORY,jobName,jobGroup);
	}

}