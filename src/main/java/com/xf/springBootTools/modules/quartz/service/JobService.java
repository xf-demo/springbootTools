package com.xf.springBootTools.modules.quartz.service;

import com.xf.springBootTools.common.pojo.core.ResponseData;
import com.xf.springBootTools.modules.quartz.dto.JobDetailPojo;

/**
 * @author xiaofeng
 * @date 2018/2/2
 */
public interface JobService {
	/**
	 * 新增
	 * @param jobDetailPojo
	 * @return
	 */
	ResponseData addJob(JobDetailPojo jobDetailPojo);

	/**
	 * 更新
	 * @param jobDetailPojo
	 * @return
	 */
	ResponseData updateJob(JobDetailPojo jobDetailPojo);
}
