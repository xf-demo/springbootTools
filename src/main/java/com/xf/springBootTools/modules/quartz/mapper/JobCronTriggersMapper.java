package com.xf.springBootTools.modules.quartz.mapper;


import com.xf.springBootTools.config.mybatis.MyMapper;
import com.xf.springBootTools.modules.quartz.dto.JobAndTrigger;
import com.xf.springBootTools.modules.quartz.dto.QrtzCronTriggers;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xiaofeng
 * @date 2018/1/29
 */
@Repository
public interface JobCronTriggersMapper extends MyMapper<QrtzCronTriggers> {
	/**
	 * 更新触发器表
	 * @param qrtzCronTriggers
	 * @return
	 */
	int updateCronTrigger(QrtzCronTriggers qrtzCronTriggers);
}
