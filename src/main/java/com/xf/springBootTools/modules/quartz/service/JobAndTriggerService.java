package com.xf.springBootTools.modules.quartz.service;


import com.github.pagehelper.PageInfo;
import com.xf.springBootTools.common.service.BaseService;
import com.xf.springBootTools.modules.quartz.dto.JobAndTrigger;

import java.util.List;

/**
 * 定时任务Service
 * @author xiaofeng
 * @date 2018/1/29
 */
public interface JobAndTriggerService extends BaseService<JobAndTrigger>{
	/**
	 * 任务列表查询
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	List<JobAndTrigger> getJobAndTriggerDetails(int pageNum, int pageSize);

	/**
	 * 任务详细查询
	 * @param optionType
	 * @param jobName
	 * @param jobGroup
	 * @return
	 */
	JobAndTrigger findJobDetail(String optionType, String jobName, String jobGroup);
}
