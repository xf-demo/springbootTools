package com.xf.springBootTools.modules.quartz.constant;

/**
 * 定时任务常量类
 *
 * @author xiaofeng
 * @date 2018/2/5
 */
public class QuartzConstant {
	/**
	 * 查看操作
	 */
	public final static String OPT_TYPE_VIEW = "view";
	/**
	 * 创建操作
	 */
	public final static String OPT_TYPE_CREATE = "create";

	/**
	 * 更新操作
	 */
	public final static String OPT_TYPE_UPDATE = "update";

	/**
	 * 删除操作
	 */
	public final static String OPT_TYPE_DELETE = "delete";

	/**
	 * SCHEDULER_FACTORY
	 */
	public final static String DEFAULT_SCHEDULER_FACTORY = "schedulerFactory";

	/**
	 * DEFAULT_GROUP
	 */
	public final static String DEFAULT_GROUP = "DEFAULT";
}
