package com.xf.springBootTools.modules.quartz.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xf.springBootTools.common.pojo.core.ResponseData;
import com.xf.springBootTools.common.service.impl.BaseServiceImpl;
import com.xf.springBootTools.exceptions.JobFileNotFoundException;
import com.xf.springBootTools.job.AbstractJob;
import com.xf.springBootTools.modules.quartz.dto.JobDetailPojo;
import com.xf.springBootTools.modules.quartz.dto.QrtzJobHistory;
import com.xf.springBootTools.modules.quartz.mapper.JobHistoryMapper;
import com.xf.springBootTools.modules.quartz.service.JobHistoryService;
import com.xf.springBootTools.modules.quartz.service.JobService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author xiaofeng
 * @date 2018/2/2
 */
@Service
public class JobDetailServiceImpl extends BaseServiceImpl<QrtzJobHistory> implements JobHistoryService {
	private static Logger log = LoggerFactory.getLogger(JobDetailServiceImpl.class);

	@Override
	public ResponseData queryJobDetail(String jobName,String jobGroup,Integer page,Integer pageNmuber) {
		QrtzJobHistory history = new QrtzJobHistory();
		history.setJobName(jobName);
		history.setJobGroup(jobGroup);
		List<QrtzJobHistory> list = select(history,page,pageNmuber);
		return new ResponseData(list);
	}
}
