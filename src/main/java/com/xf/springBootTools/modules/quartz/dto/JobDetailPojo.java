package com.xf.springBootTools.modules.quartz.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.xf.springBootTools.common.util.CustomDateDeserialize;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.util.Date;

/**
 * 接收定时任务提交的表单参数实体类</br>
 * 增加了时间转换
 *
 * @author xiaofeng
 * @date 2018/1/29
 */
public class JobDetailPojo {
	private String jobName;
	private String jobGroup;
	private String jobClassName;
	private String description;
	private String cronExpression;
	@JsonDeserialize(using = CustomDateDeserialize.class)
	@NotNull
	private Long startTime;
	@NotNull
	@JsonDeserialize(using = CustomDateDeserialize.class)
	private Long endTime;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}

	public String getJobClassName() {
		return jobClassName;
	}

	public void setJobClassName(String jobClassName) {
		this.jobClassName = jobClassName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
}
