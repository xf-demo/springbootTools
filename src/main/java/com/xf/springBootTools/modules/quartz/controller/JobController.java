package com.xf.springBootTools.modules.quartz.controller;

import com.xf.springBootTools.common.pojo.core.ResponseData;
import com.xf.springBootTools.modules.quartz.dto.JobAndTrigger;
import com.xf.springBootTools.modules.quartz.dto.JobDetailPojo;
import com.xf.springBootTools.modules.quartz.service.JobAndTriggerService;
import com.xf.springBootTools.modules.quartz.service.JobHistoryService;
import com.xf.springBootTools.modules.quartz.service.JobService;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author xiaofeng
 */
@Controller
@RequestMapping(value = "/job")
public class JobController {
	@Autowired
	private JobAndTriggerService jobAndTriggerService;

	@Autowired
	private JobService jobService;

	@Autowired
	@Qualifier("scheduler")
	private Scheduler scheduler;

	@Autowired
	private JobHistoryService jobHistoryService;

	/**
	 * 跳转job页面
	 *
	 * @return
	 */
	@RequestMapping("/index")
	public String jobIndex() {
		return "job/jobList";
	}

	/**
	 * 跳转job运行详情页面
	 *
	 * @return
	 */
	@RequestMapping("/detail")
	public String jobDetail(
	                      @RequestParam(required = false) String jobName,
	                      @RequestParam(required = false) String jobGroup,
	                      Model model) {
		model.addAttribute("jobName", jobName);
		model.addAttribute("jobGroup", jobGroup);
		return "job/jobDetail";
	}

	/**
	 * 跳转job页面
	 *
	 * @return
	 */
	@RequestMapping("/edit")
	public String jobEdit(@RequestParam(required = false) String optionType,
	                      @RequestParam(required = false) String jobName,
	                      @RequestParam(required = false) String jobGroup,
	                      Model model) {
		JobAndTrigger job = jobAndTriggerService.findJobDetail(optionType, jobName, jobGroup);
		model.addAttribute("job", job);
		model.addAttribute("optionType", optionType);
		return "job/jobEdit";
	}

	/**
	 * 新建job
	 *
	 * @throws Exception
	 */
	@PostMapping(value = "/submit")
	@ResponseBody
	public ResponseData addJob(@RequestBody JobDetailPojo jobDetailPojo) {
		return jobService.addJob(jobDetailPojo);
	}

	/**
	 * 更新job
	 *
	 * @throws Exception
	 */
	@PostMapping(value = "/update")
	@ResponseBody
	public ResponseData update(@RequestBody JobDetailPojo jobDetailPojo) {
		return jobService.updateJob(jobDetailPojo);
	}

	/**
	 * 暂停
	 *
	 * @param jobAndTriggers
	 * @throws Exception
	 */
	@PostMapping(value = "/pauseJob")
	@ResponseBody
	public ResponseData pauseJob(@RequestBody List<JobAndTrigger> jobAndTriggers) throws Exception {
		for (JobAndTrigger jobAndTrigger : jobAndTriggers){
			scheduler.pauseJob(JobKey.jobKey(jobAndTrigger.getJobName(), jobAndTrigger.getJobGroup()));
		}
		return new ResponseData();

	}

	/**
	 * 重启
	 *
	 * @param jobAndTriggers
	 * @throws Exception
	 */
	@PostMapping(value = "/resumeJob")
	@ResponseBody
	public ResponseData resumeJob(@RequestBody List<JobAndTrigger> jobAndTriggers) throws Exception {
		for (JobAndTrigger jobAndTrigger : jobAndTriggers){
			scheduler.resumeJob(JobKey.jobKey(jobAndTrigger.getJobName(), jobAndTrigger.getJobGroup()));
		}
		return new ResponseData();
	}

	@PostMapping(value = "/rescheduleJob")
	@ResponseBody
	public void rescheduleJob(@RequestParam(value = "jobClassName") String jobClassName,
	                          @RequestParam(value = "jobGroupName") String jobGroupName,
	                          @RequestParam(value = "cronExpression") String cronExpression) throws Exception {
		jobReschedule(jobClassName, jobGroupName, cronExpression);
	}

	@PostMapping(value = "/deleteJob")
	@ResponseBody
	public void deleteJob(@RequestParam(value = "jobClassName") String jobClassName, @RequestParam(value = "jobGroupName") String jobGroupName) throws Exception {
		scheduler.pauseTrigger(TriggerKey.triggerKey(jobClassName, jobGroupName));
		scheduler.unscheduleJob(TriggerKey.triggerKey(jobClassName, jobGroupName));
		scheduler.deleteJob(JobKey.jobKey(jobClassName, jobGroupName));
	}

	/**
	 * 查询任务列表
	 *
	 * @param page
	 * @param limit
	 * @return
	 */
	@GetMapping(value = "/queryJob")
	@ResponseBody
	public ResponseData queryJob(@RequestParam(value = "page") Integer page, @RequestParam(value = "limit") Integer limit) {
		List<JobAndTrigger> jobAndTrigger = jobAndTriggerService.getJobAndTriggerDetails(page, limit);
		return new ResponseData(jobAndTrigger);
	}

	/**
	 * 查询任务执行情况
	 *
	 * @param page
	 * @param limit
	 * @param jobGroup
	 * @param jobName
	 * @return
	 */
	@GetMapping(value = "/queryJobDetail")
	@ResponseBody
	public ResponseData queryJobDetail(
										@RequestParam(value = "page") Integer page,
										@RequestParam(value = "limit") Integer limit,
										@RequestParam(value = "jobName") String jobName,
										@RequestParam(value = "jobGroup") String jobGroup) {
		return jobHistoryService.queryJobDetail(jobName,jobGroup,page,limit);
	}

	public void jobReschedule(String jobClassName, String jobGroupName, String cronExpression) throws Exception {
		try {
			TriggerKey triggerKey = TriggerKey.triggerKey(jobClassName, jobGroupName);
			// 表达式调度构建器
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
			CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
			// 按新的cronExpression表达式重新构建trigger
			trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
			// 按新的trigger重新设置job执行
			scheduler.rescheduleJob(triggerKey, trigger);
		} catch (SchedulerException e) {
			System.out.println("更新定时任务失败" + e);
			throw new Exception("更新定时任务失败");
		}
	}

}
