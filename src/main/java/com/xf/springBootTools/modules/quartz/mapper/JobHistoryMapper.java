package com.xf.springBootTools.modules.quartz.mapper;

import com.xf.springBootTools.config.mybatis.MyMapper;
import com.xf.springBootTools.modules.quartz.dto.JobAndTrigger;
import com.xf.springBootTools.modules.quartz.dto.QrtzJobHistory;
import org.springframework.stereotype.Repository;

/**
 * @author xiaofeng
 * @date 2018/1/29
 */
@Repository
public interface JobHistoryMapper extends MyMapper<QrtzJobHistory> {

}
