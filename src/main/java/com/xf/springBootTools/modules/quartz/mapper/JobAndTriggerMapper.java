package com.xf.springBootTools.modules.quartz.mapper;


import com.xf.springBootTools.config.mybatis.MyMapper;
import com.xf.springBootTools.modules.quartz.dto.JobAndTrigger;
import com.xf.springBootTools.modules.quartz.dto.QrtzCronTriggers;
import com.xf.springBootTools.modules.sysUser.dto.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author xiaofeng
 * @date 2018/1/29
 */
@Repository
public interface JobAndTriggerMapper extends MyMapper<JobAndTrigger> {
	/**
	 * 任务列表查询
	 *
	 * @return JobAndTrigger集合
	 */
	List<JobAndTrigger> getJobAndTriggerDetails();

	/**
	 * 任务详细查询<br/>
	 * 这3个参数为定时任务明细表的主键约束
	 *
	 * @param schedName 默认调度器
	 * @param jobName   任务名称
	 * @param jobGroup  任务组
	 * @return JobAndTrigger对象
	 */
	JobAndTrigger findJobDetail(@Param("schedName") String schedName, @Param("jobName") String jobName, @Param("jobGroup") String jobGroup);

	/**
	 * 更新任务执行Class
	 * @param jobAndTrigger
	 * @returne
	 */
	int updateJobExecuteClassAndDescription(JobAndTrigger jobAndTrigger);
}
