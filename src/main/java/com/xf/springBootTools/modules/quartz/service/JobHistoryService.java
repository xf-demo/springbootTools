package com.xf.springBootTools.modules.quartz.service;

import com.xf.springBootTools.common.pojo.core.ResponseData;
import com.xf.springBootTools.common.service.BaseService;
import com.xf.springBootTools.modules.quartz.dto.JobDetailPojo;
import com.xf.springBootTools.modules.quartz.dto.QrtzJobHistory;

/**
 * @author xiaofeng
 * @date 2018/2/2
 */
public interface JobHistoryService extends BaseService<QrtzJobHistory> {

	ResponseData queryJobDetail(String jobName,String jobGroup,Integer page,Integer pageNmuber);
}
