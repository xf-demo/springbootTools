package com.xf.springBootTools.modules.quartz.dto;

import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * @author xiaofeng
 * @date 2018/1/29
 * <p>
 * 联合主键。一般使用PK 只需要定义用作主键的字段
 * <p>
 * 联合主键类必须遵守的JPA规范：<br>
 * 1、必须要提供一个public的无参数的构造方法<br>
 * 2、必须要实现序列化接口<br>
 * 3、必须重写hashCode（）与equals（）方法
 */
// 用在实体里面,只是使用该类中的属性。（该类中的属性用在持久化的类中的字段）
@Embeddable
public class QrtzTriggersPk implements Serializable {
	private String schedName;
	private String triggerName;
	private String triggerGroup;

	public QrtzTriggersPk(String schedName, String triggerName, String triggerGroup) {
		this.schedName = schedName;
		this.triggerName = triggerName;
		this.triggerGroup = triggerGroup;
	}

	public QrtzTriggersPk() {
	}

	public String getSchedName() {
		return schedName;
	}

	public void setSchedName(String schedName) {
		this.schedName = schedName;
	}

	public String getTriggerName() {
		return triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	public String getTriggerGroup() {
		return triggerGroup;
	}

	public void setTriggerGroup(String triggerGroup) {
		this.triggerGroup = triggerGroup;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		QrtzTriggersPk that = (QrtzTriggersPk) o;
		if (schedName != null ? !schedName.equals(that.schedName) : that.schedName != null) {
			return false;
		}
		if (triggerName != null ? !triggerName.equals(that.triggerName) : that.triggerName != null) {
			return false;
		}
		return triggerGroup != null ? triggerGroup.equals(that.triggerGroup) : that.triggerGroup == null;
	}

	@Override
	public int hashCode() {
		int result = schedName != null ? schedName.hashCode() : 0;
		result = 31 * result + (triggerName != null ? triggerName.hashCode() : 0);
		result = 31 * result + (triggerGroup != null ? triggerGroup.hashCode() : 0);
		return result;
	}
}
