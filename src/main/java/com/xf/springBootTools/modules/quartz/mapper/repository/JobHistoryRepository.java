package com.xf.springBootTools.modules.quartz.mapper.repository;

import com.xf.springBootTools.modules.quartz.dto.QrtzJobHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * @author xiaofeng
 * @date 2018/1/29
 */
@Repository
public interface JobHistoryRepository extends JpaRepository<QrtzJobHistory, Long> {
}
