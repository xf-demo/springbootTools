package com.xf.springBootTools.modules.quartz.service.impl;

import com.xf.springBootTools.common.pojo.core.ResponseData;
import com.xf.springBootTools.common.service.impl.BaseServiceImpl;
import com.xf.springBootTools.exceptions.JobFileNotFoundException;
import com.xf.springBootTools.job.AbstractJob;
import com.xf.springBootTools.modules.quartz.constant.QuartzConstant;
import com.xf.springBootTools.modules.quartz.dto.JobAndTrigger;
import com.xf.springBootTools.modules.quartz.dto.JobDetailPojo;
import com.xf.springBootTools.modules.quartz.dto.QrtzCronTriggers;
import com.xf.springBootTools.modules.quartz.dto.QrtzJobHistory;
import com.xf.springBootTools.modules.quartz.mapper.JobAndTriggerMapper;
import com.xf.springBootTools.modules.quartz.mapper.JobCronTriggersMapper;
import com.xf.springBootTools.modules.quartz.service.JobAndTriggerService;
import com.xf.springBootTools.modules.quartz.service.JobService;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * @author xiaofeng
 * @date 2018/2/2
 */
@Service
public class JobServiceImpl implements JobService {
	private static Logger log = LoggerFactory.getLogger(JobServiceImpl.class);
	@Autowired
	@Qualifier("scheduler")
	private Scheduler scheduler;

	@Autowired
	private JobAndTriggerService jobAndTriggerService;

	@Autowired
	private JobAndTriggerMapper jobAndTriggerMapper;

	@Autowired
	private JobCronTriggersMapper jobCronTriggersMapper;

	@Override
	public ResponseData addJob(JobDetailPojo jobDetailPojo) {
		try {
			String jobGroupName = jobDetailPojo.getJobGroup();
			String jobName = jobDetailPojo.getJobName();
			String jobClassName = jobDetailPojo.getJobClassName();
			String cronExpression = jobDetailPojo.getCronExpression();
			Long startTime = jobDetailPojo.getStartTime();
			Long endTime = jobDetailPojo.getEndTime();
			// 启动调度器
			scheduler.start();
			//构建job信息
			if (StringUtils.isEmpty(jobGroupName)) {
				jobGroupName = QuartzConstant.DEFAULT_GROUP;
			}
			JobDetail jobDetail = JobBuilder.newJob(getClass(jobClassName).getClass()).withIdentity(jobName, jobGroupName).build();
			//表达式调度构建器(即任务执行的时间)
			CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression);
			//按新的cronExpression表达式构建一个新的trigger
			CronTrigger trigger = TriggerBuilder.newTrigger()
					.withIdentity(jobName, jobGroupName)
					.withSchedule(scheduleBuilder)
					.startAt(new Date(startTime))
					.endAt(new Date(endTime))
					.build();
			scheduler.scheduleJob(jobDetail, trigger);
		} catch (SchedulerException e) {
			if (e instanceof ObjectAlreadyExistsException) {
				return new ResponseData(false, "相同定时任务已存在");
			}
			log.error("创建定时任务失败", e);
			return new ResponseData(false, e.getMessage());
		}
		return new ResponseData();
	}

	/**
	 * 更新任务
	 * @param jobDetailPojo
	 * @return
	 */
	@Override
	public ResponseData updateJob(JobDetailPojo jobDetailPojo) {
		try {
			String jobGroupName = jobDetailPojo.getJobGroup();
			String jobName = jobDetailPojo.getJobName();
			String jobClassName = jobDetailPojo.getJobClassName();
			String description = jobDetailPojo.getDescription();
			String cronExpression = jobDetailPojo.getCronExpression();
			//校验任务是否存在
			JobAndTrigger jt = jobAndTriggerService.findJobDetail(QuartzConstant.OPT_TYPE_UPDATE, jobName, jobGroupName);
			if (jt == null) {
				return new ResponseData(false, "未找到要修改等任务");
			}
			//更新任务主表
			JobAndTrigger jobAndTrigger = new JobAndTrigger();
			jobAndTrigger.setDescription(description);
			jobAndTrigger.setJobClassName(jobClassName);
			jobAndTrigger.setSchedName(QuartzConstant.DEFAULT_SCHEDULER_FACTORY);
			jobAndTrigger.setJobName(jobName);
			jobAndTrigger.setJobGroup(jobGroupName);
			jobAndTriggerMapper.updateJobExecuteClassAndDescription(jobAndTrigger);
			//更新触发器表
			QrtzCronTriggers qrtzCronTriggers = new QrtzCronTriggers();
			qrtzCronTriggers.setSchedName(QuartzConstant.DEFAULT_SCHEDULER_FACTORY);
			qrtzCronTriggers.setTriggerName(jobName);
			qrtzCronTriggers.setTriggerGroup(jobGroupName);
			qrtzCronTriggers.setCronExpression(cronExpression);
			jobCronTriggersMapper.updateCronTrigger(qrtzCronTriggers);
			return new ResponseData();
		}catch (Exception e){
			log.error("任务更新失败",e);
			return new ResponseData(false,e.getMessage());
		}
	}

	public static AbstractJob getClass(String classname) {
		Class<?> class1 = null;
		try {
			class1 = Class.forName(classname);
			return (AbstractJob) class1.newInstance();
		} catch (Exception e) {
			log.error("", e);
			throw new JobFileNotFoundException(JobFileNotFoundException.JOB_FILE_NOT_FOUND);
		}
	}
}
