package com.xf.springBootTools.modules.generator.dto;

import java.util.ArrayList;
import java.util.List;

public class DBTable {
    private String name;
    private List<DBColumn> columns;

    public DBTable() {
        columns = new ArrayList<DBColumn>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DBColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<DBColumn> columns) {
        this.columns = columns;
    }

}
