package com.xf.springBootTools.modules.generator.controllers;

import com.xf.springBootTools.common.pojo.core.ResponseData;
import com.xf.springBootTools.modules.generator.dto.GeneratorInfo;
import com.xf.springBootTools.modules.generator.service.GeneratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 代码生成器
 */
@Controller
@RequestMapping(value = "/generator")
public class GeneratorController {
	@Autowired
	@Qualifier("generatorServiceImpl")
	private GeneratorService service;

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(Model model) {
		List<String> tables =  service.showTables();
		model.addAttribute("tables",tables);
		return "generator/index";
	}

	@RequestMapping(value = "/alltables", method = RequestMethod.GET)
	@ResponseBody
	public ResponseData showTables() {
		return new ResponseData(service.showTables());
	}

	/**
	 * 生成代码
	 *
	 * @param generatorInfo
	 * @return
	 */
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	@ResponseBody
	public int generatorTables(GeneratorInfo generatorInfo, HttpServletRequest request) {
		int rs = service.generatorFile(generatorInfo,request);
		return rs;
	}

}
