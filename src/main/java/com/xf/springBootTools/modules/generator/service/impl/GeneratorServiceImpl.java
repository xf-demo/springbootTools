package com.xf.springBootTools.modules.generator.service.impl;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.xf.springBootTools.modules.generator.dto.DBColumn;
import com.xf.springBootTools.modules.generator.dto.DBTable;
import com.xf.springBootTools.modules.generator.dto.GeneratorInfo;
import com.xf.springBootTools.modules.generator.service.GeneratorService;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


import freemarker.template.TemplateException;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

@Service
public class GeneratorServiceImpl implements GeneratorService {

	@Autowired
	@Qualifier("sqlSessionFactory")
	SqlSessionFactory sqlSessionFactory;

	private Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 查询表
	 */
	@Override
	public List<String> showTables() {
		Connection conn = null;
		try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
			List<String> tables;
			conn = DBUtil.getConnectionBySqlSession(sqlSession);
			tables = DBUtil.showAllTables(conn);
			return tables;
		} catch (SQLException e) {
			logger.error("sql query error", e);
			return new ArrayList<String>();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				logger.error("connection close error", e);
			}
		}
	}

	/**
	 * 文件生成
	 */
	@Override
	public int generatorFile(GeneratorInfo info, HttpServletRequest request) {
		int rs = 0;
		String tableName = info.getTargetName();
		DBTable dbTable = getTableInfo(tableName);
		try {
			rs = createFile(dbTable, info, request);
		} catch (IOException e) {
			rs = -1;
			logger.error("", e);
		} catch (TemplateException e) {
			rs = -1;
			logger.error("", e);
		}
		return rs;
	}

	/**
	 * 汇总表字段信息
	 */
	public DBTable getTableInfo(String tableName) {
		Connection conn = null;
		ResultSet rs = null;
		DBTable dbTable = new DBTable();
		List<DBColumn> columns = dbTable.getColumns();
		List<String> NotNullColumns = null;
		try {
			SqlSession sqlSession = sqlSessionFactory.openSession();
			// 设置tableName
			dbTable.setName(tableName);
			conn = DBUtil.getConnectionBySqlSession(sqlSession);
			DatabaseMetaData dbmd = conn.getMetaData();
			// 获取主键字段
			String columnPk = DBUtil.getPrimaryKey(tableName, dbmd);
			// 获取不为空的字段
			NotNullColumns = DBUtil.getNotNullColumn(tableName, dbmd);
			// 获取表列信息
			rs = DBUtil.getTableColumnInfo(tableName, dbmd);
			while (rs.next()) {
				String columnName = rs.getString("COLUMN_NAME");
				if ("OBJECT_VERSION_NUMBER".equalsIgnoreCase(columnName) || "REQUEST_ID".equalsIgnoreCase(columnName)
						|| "PROGRAM_ID".equalsIgnoreCase(columnName) || "CREATED_BY".equalsIgnoreCase(columnName)
						|| "CREATION_DATE".equalsIgnoreCase(columnName) || "LAST_UPDATED_BY".equalsIgnoreCase(columnName)
						|| "LAST_UPDATE_DATE".equalsIgnoreCase(columnName) || "LAST_UPDATE_LOGIN".equalsIgnoreCase(columnName)
						|| columnName.toUpperCase().startsWith("ATTRIBUTE")) {
					continue;
				}
				columns.add(setColumnInfo(rs, columnPk, NotNullColumns));
			}
		} catch (SQLException e) {
			logger.error("sql exception", e);
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				logger.error("ResultSet close exception", e);
			}
			try {
				conn.close();
			} catch (SQLException e) {
				logger.error("Connection close exception", e);
			}
		}
		return dbTable;
	}

	private DBColumn setColumnInfo(ResultSet rs1, String columnPk, List<String> NotNullColumns) throws SQLException {
		DBColumn column = new DBColumn();
		String columnName = rs1.getString("COLUMN_NAME");
		column.setName(columnName);
		String typeName = rs1.getString("TYPE_NAME");
		column.setJdbcType(typeName);
		if (!StringUtils.isEmpty(rs1.getString("REMARKS"))) {
			column.setRemarks(rs1.getString("REMARKS"));
		}
		// 判断是否为主键
		if (columnName.equalsIgnoreCase(columnPk)) {
			column.setId(true);
		}
		// 判断是否为null字段
		for (String n : NotNullColumns) {
			if (columnName.equalsIgnoreCase(n) && !columnName.equalsIgnoreCase(columnPk)) {
				if ("BIGINT".equalsIgnoreCase(typeName)) {
					column.setNotNull(true);
				} else if ("VARCHAR".equalsIgnoreCase(typeName)) {
					column.setNotEmpty(true);
				}
			}
		}
		column.setColumnLength(rs1.getString("COLUMN_SIZE"));
		return column;
	}

	/**
	 * 创建文件
	 * @param table
	 * @param info
	 * @param request
	 * @return
	 * @throws IOException
	 * @throws TemplateException
	 */
	public int createFile(DBTable table, GeneratorInfo info, HttpServletRequest request) throws IOException, TemplateException {
		int rs = FileUtil.isFileExist(info);
		//无操作
		String notOperation = "NotOperation";
		if (rs == 0) {
			if (!notOperation.equalsIgnoreCase(info.getDtoStatus())) {
				FileUtil.createDto(table, info);
			}
			if (!notOperation.equalsIgnoreCase(info.getControllerStatus())) {
				FileUtil.createFtlInfoByType(FileUtil.pType.Controller, table, info, request);
			}
			if (!notOperation.equalsIgnoreCase(info.getMapperStatus())) {
				FileUtil.createFtlInfoByType(FileUtil.pType.Mapper, table, info, request);
			}
			if (!notOperation.equalsIgnoreCase(info.getImplStatus())) {
				FileUtil.createFtlInfoByType(FileUtil.pType.Impl, table, info, request);
			}
			if (!notOperation.equalsIgnoreCase(info.getServiceStatus())) {
				FileUtil.createFtlInfoByType(FileUtil.pType.Service, table, info, request);
			}
			if (!notOperation.equalsIgnoreCase(info.getMapperXmlStatus())) {
				FileUtil.createFtlInfoByType(FileUtil.pType.MapperXml, table, info, request);
			}
			//html页面暂时不生成
			if (!notOperation.equalsIgnoreCase(info.getHtmlStatus())) {
				// FileUtil.createFtlInfoByType(FileUtil.pType.Html, table, info);
			}
		}
		return rs;
	}

}
