package com.xf.springBootTools.modules.generator.service;


import com.xf.springBootTools.modules.generator.dto.GeneratorInfo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface GeneratorService {
	List<String> showTables();

	int generatorFile(GeneratorInfo info, HttpServletRequest request);

}
