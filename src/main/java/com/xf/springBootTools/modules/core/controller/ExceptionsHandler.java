package com.xf.springBootTools.modules.core.controller;

import com.xf.springBootTools.common.pojo.core.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * @author zhuweifeng
 * @date 2017/11/2
 */
@ControllerAdvice
@ResponseBody
public class ExceptionsHandler {
	Logger logger = LoggerFactory.getLogger(ExceptionsHandler.class);

	/**
	 * 自定义异常处理方法，如果有自定义异常可在这里添加错误编码，当然也可以自己定义一个编码在Result里返回
	 * 目的：将未捕捉的异常统一包装处理，防止直接将堆栈输出给调用方
	 *
	 * @return
	 */
	@ExceptionHandler
	public ResponseData runtimeExceptionHandler(Exception exception) {
		String msgCode = null;
		if (exception instanceof IOException) {
			msgCode = "IO_ERROR";
		} else {
			msgCode = exception.getMessage();
		}
		logger.error("", exception);
		return new ResponseData(false, msgCode);
	}
}
