package com.xf.springBootTools.modules.core.dao.repository;

import com.xf.springBootTools.common.pojo.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    /**
     * 根据登录名查找用户
     * @param loginName
     * @return
     */
    User findUserByLoginName(String loginName);

}
