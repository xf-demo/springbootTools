package com.xf.springBootTools.modules.core.controller;

import com.xf.springBootTools.modules.core.dao.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;

/**
 * @author zhuweifeng
 * @date 2017/10/30
 */
@Controller
public class LoginController {
	@Autowired
	private UserRepository userRepository;

	/**
	 * 跳转到首页
	 * @return
	 */
	@GetMapping("/")
	public String index() {
		return "index/index";
	}
	/**
	 * 跳转到首页
	 * @return
	 */
	@GetMapping("/index")
	public String indexPage(Model model) {
		model.addAttribute("sysname","后台管理系统");
		return "index/index";
	}

	/**
	 * 跳转到登录页
	 * @return
	 */
	@GetMapping("/login")
	public String loginPage(Model model) {
		model.addAttribute("sysname","xxx系统");
		return "login";
	}

	/**
	 * 加载main
	 * @return
	 */
	@GetMapping("/main")
	public String toMain(HttpServletResponse response) {
		return "index/main";
	}

}
