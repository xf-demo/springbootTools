package com.xf.springBootTools.modules.core.dao.repository;

import com.xf.springBootTools.common.pojo.dto.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {

}