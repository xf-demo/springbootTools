package com.xf.springBootTools.common.exception;


import com.xf.springBootTools.common.pojo.dto.BaseDTO;

/**
 * 通用更新操作,更新失败:记录不存在,OBJECT_VERSION_NUMBER 不匹配。
 *
 * @author
 */
public class ColumnNullException extends BaseException {

	public static final String MESSAGE_KEY = "error.column can't null";

	protected ColumnNullException(String code, String descriptionKey, Object[] parameters) {
		super(code, descriptionKey, parameters);
	}

	public ColumnNullException() {
		this("SYS", MESSAGE_KEY, null);
	}
}
