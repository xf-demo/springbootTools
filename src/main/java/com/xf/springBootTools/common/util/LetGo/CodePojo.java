package com.xf.springBootTools.common.util.LetGo;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class CodePojo {
	private String err_no;
	private String err_str;
	private String pic_id;
	private String pic_str;
	private String md5;

	public String getErr_no() {
		return err_no;
	}

	public void setErr_no(String err_no) {
		this.err_no = err_no;
	}

	public String getErr_str() {
		return err_str;
	}

	public void setErr_str(String err_str) {
		this.err_str = err_str;
	}

	public String getPic_id() {
		return pic_id;
	}

	public void setPic_id(String pic_id) {
		this.pic_id = pic_id;
	}

	public String getPic_str() {
		return pic_str;
	}

	public void setPic_str(String pic_str) {
		this.pic_str = pic_str;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}
}
