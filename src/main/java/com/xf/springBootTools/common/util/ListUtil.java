package com.xf.springBootTools.common.util;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author zhuweifeng
 * @date 2017/9/5
 */
public class ListUtil {
	/**
	 * 创建一个List集合
	 *
	 * @param t
	 * @return
	 */
	public static <T> List<T> createList(T t) {
		List<T> list = new ArrayList<T>();
		list.add(t);
		return list;
	}

	/**
	 * 创建一个空的List集合
	 *
	 * @return
	 */
	public static <T> List<T> createEmptyList() {
		List<T> list = new ArrayList<T>();
		return list;
	}


	/**
	 * List<String>集合去重
	 */
	public static List<String> distinct(List<String> list) {
	    if(!CollectionUtils.isEmpty(list)){
            Set set = new HashSet(list);
            list = new ArrayList(set);
        }
		return list;
	}

	/**
	 * 对list集合进行分页处理
	 *
	 * @return
	 */
	public static <T> List<T> listSplit(List<T> list, Integer pageSize, Integer page) {
		List<T> newList = new ArrayList<>();
		Integer total = list.size();
		newList = list.subList(pageSize * (page - 1), ((pageSize * page) > total ? total : (pageSize * page)));
		return newList;
	}

}
