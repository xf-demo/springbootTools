package com.xf.springBootTools.common.util;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * rabbitmq队列操作工具类
 * @author zhuweifeng
 * @date 2017/11/2
 */
@Component
public class MessageSender {
	@Autowired
	private RabbitTemplate rabbitTemplate;

	/**
	 * 队列发送消息
	 * @param routingKey
	 * @param msg
	 */
	public void sendMessage(String routingKey,String msg){
		rabbitTemplate.convertAndSend(routingKey,msg);
	}
}
