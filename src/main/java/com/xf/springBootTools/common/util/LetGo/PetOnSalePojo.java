package com.xf.springBootTools.common.util.LetGo;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class PetOnSalePojo {
	private String id;
	private String petId;
	private Integer birthType;
	private Integer mutation;
	private Integer generation;
	private Integer rareDegree;
	private String desc;
	private Integer petType;
	private Double amount;
	private String bgColor;
	private String petUrl;
	private String validCode;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPetId() {
		return petId;
	}

	public void setPetId(String petId) {
		this.petId = petId;
	}

	public Integer getBirthType() {
		return birthType;
	}

	public void setBirthType(Integer birthType) {
		this.birthType = birthType;
	}

	public Integer getMutation() {
		return mutation;
	}

	public void setMutation(Integer mutation) {
		this.mutation = mutation;
	}

	public Integer getGeneration() {
		return generation;
	}

	public void setGeneration(Integer generation) {
		this.generation = generation;
	}

	public Integer getRareDegree() {
		return rareDegree;
	}

	public void setRareDegree(Integer rareDegree) {
		this.rareDegree = rareDegree;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getPetType() {
		return petType;
	}

	public void setPetType(Integer petType) {
		this.petType = petType;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public String getBgColor() {
		return bgColor;
	}

	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getPetUrl() {
		return petUrl;
	}

	public void setPetUrl(String petUrl) {
		this.petUrl = petUrl;
	}

	public String getValidCode() {
		return validCode;
	}

	public void setValidCode(String validCode) {
		this.validCode = validCode;
	}
}
