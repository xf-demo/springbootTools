package com.xf.springBootTools.common.util.LetGo;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class GenData {
	private String seed;
	private String img;

	public String getSeed() {
		return seed;
	}

	public void setSeed(String seed) {
		this.seed = seed;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	@Override
	public String toString() {
		return "GenData{" +
				"seed='" + seed + '\'' +
				", img='" + img + '\'' +
				'}';
	}
}
