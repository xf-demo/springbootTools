package com.xf.springBootTools.common.util.LetGo;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class PetListPojo {
	private String errorNo;
	private String errorMsg;
	private String timestamp;
	private PetListDataPojo data;

	public String getErrorNo() {
		return errorNo;
	}

	public void setErrorNo(String errorNo) {
		this.errorNo = errorNo;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public PetListDataPojo getData() {
		return data;
	}

	public void setData(PetListDataPojo data) {
		this.data = data;
	}
}
