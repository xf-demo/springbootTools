package com.xf.springBootTools.common.util;

import okhttp3.*;
import okhttp3.internal.http2.Header;

import java.io.IOException;
import java.util.Map;

/**
 * OkHttpUtil工具类
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class OkHttpUtil {
	public static final OkHttpClient client = new OkHttpClient();
	public static final MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
	private final static String COOKIE = "BAIDUID=8BF4EE8F51EFCD9B6A5167CDF2BA8D6C:FG=1; BIDUPSID=8BF4EE8F51EFCD9B6A5167CDF2BA8D6C; PSTM=1517794571; BDUSS=luVjJVNTJOeFFjOFBYLVQ2S1pxUUYxcUdMQVVLMjVXaWlmYUJRRVZaTjltNTlhQVFBQUFBJCQAAAAAAAAAAAEAAACTirc3QWpheM~-t-cAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH0OeFp9DnhaZn; BDRCVFR[mkUqnUt8juD]=mk3SLVN4HKm; BDRCVFR[BCzcNGRrF63]=mk3SLVN4HKm; BDRCVFR[feWj1Vr5u3D]=I67x6TjHwwYf0; PSINO=5; H_PS_PSSID=1424_21106_22160";

	public static String run(String url) throws IOException {
		Request request = new Request.Builder()
				.url(url)
				.build();
		Response response = client.newCall(request).execute();
		return response.body().string();
	}

	public static String post(String url, String json) throws IOException {
		RequestBody body = RequestBody.create(mediaType, json);
		Request request = new Request.Builder()
				.url(url)
				.header("Cookie",COOKIE)
				.post(body)
				.build();
		Response response = client.newCall(request).execute();
		return response.body().string();
	}
}
