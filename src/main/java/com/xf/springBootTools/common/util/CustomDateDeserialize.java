package com.xf.springBootTools.common.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.xf.springBootTools.common.exception.ColumnNullException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomDateDeserialize extends JsonDeserializer<Long> {

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public Long deserialize(JsonParser paramJsonParser, DeserializationContext paramDeserializationContext) throws IOException {
		String str = paramJsonParser.getText().trim();
		if(StringUtils.isEmpty(str)){
			return 0L;
		}
		try {
			Date date = dateFormat.parse(str);
			return date.getTime();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return paramDeserializationContext.parseDate(str).getTime();
	}
} 