package com.xf.springBootTools.common.util.ocr.baiduocr;

import com.baidu.aip.ocr.AipOcr;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * 百度OCR例子
 * 官方地址 https://cloud.baidu.com/?from=console
 *
 * @author xiaofeng
 * @date 2018/2/8
 */
public class BaiduOcrDemo {
	//设置APPID/AK/SK
	public static final String APP_ID = "10809933";
	public static final String API_KEY = "RbdrPDMM8D5fGwZyXe9C71g5";
	public static final String SECRET_KEY = "GpKrXVLEFrsG1GfWljGtWnciPETgd9t0";

	public static void main(String[] args) {
		// 初始化一个AipOcr
		AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);

		// 可选：设置网络连接参数
		client.setConnectionTimeoutInMillis(2000);
		client.setSocketTimeoutInMillis(60000);

		// 可选：设置代理服务器地址, http和socket二选一，或者均不设置
		//client.setHttpProxy("proxy_host", proxy_port);  // 设置http代理
		//client.setSocketProxy("proxy_host", proxy_port);  // 设置socket代理

		// 可选：设置log4j日志输出格式，若不设置，则使用默认配置
		// 也可以直接通过jvm启动参数设置此环境变量
		//System.setProperty("aip.log4j.conf", "path/to/your/log4j.properties");

		// 调用接口
		String path =  "/Users/zhuweifeng/Downloads/1111/qqq.png";
		JSONObject res = client.basicGeneral(path, new HashMap<String, String>());
		System.out.println(res.toString(2));

	}
}
