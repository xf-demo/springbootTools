package com.xf.springBootTools.common.util;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


public class Product {
	private final static String QUEUE_NAME = "hello";

	public static void main(String[] args) {
		Connection connection = null;
		Channel channel = null;
		try {
			//创建连接工厂
			ConnectionFactory factory = new ConnectionFactory();
			//设置RabbitMQ相关信息
			factory.setHost("10.211.55.4");
			factory.setUsername("admin");
			factory.setPassword("123123");
			factory.setPort(5672);
			//创建一个新的连接
			connection = factory.newConnection();
			//创建一个通道
			channel = connection.createChannel();
			// 声明一个队列
			String message = "Hello RabbitMQ11111";
			//发送消息到队列中
			channel.queueDeclare(QUEUE_NAME, false, false, false, null);
			channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
			System.out.println("Producer Send +'" + message + "'");
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			//关闭通道和连接
			try {
				channel.close();
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}