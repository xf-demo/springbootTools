package com.xf.springBootTools.common.util;

import sun.misc.BASE64Decoder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Base64转换工具
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class Base64Util {

	/**
	 * @param imgStr base64编码字符串
	 * @param path   图片路径-具体到文件
	 * @return
	 * @Description: 将base64编码字符串转换为图片
	 * @Author:
	 * @CreateTime:
	 */
	public static boolean generateImage(String imgStr, String path) throws IOException {
		OutputStream out = null;
		if (imgStr == null){
			return false;
		}
		BASE64Decoder decoder = new BASE64Decoder();
		try {
			// 解密
			byte[] b = decoder.decodeBuffer(imgStr);
			// 处理数据
			for (int i = 0; i < b.length; ++i) {
				if (b[i] < 0) {
					b[i] += 256;
				}
			}
			out = new FileOutputStream(path);
			out.write(b);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			if(out!=null){
				out.flush();
				out.close();
			}
		}
	}

	public static void main(String[] args) throws IOException {
		String base64str = "/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCABaAPoDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD0PwX4L8K3XgXw9cXHhrRpp5dMtnkkksImZ2MSkkkrkknnNbn/AAgng/8A6FTQ/wDwXQ//ABNHgT/knnhr/sFWv/opa6CgDn/+EE8H/wDQqaH/AOC6H/4mj/hBPB//AEKmh/8Aguh/+JroKKAOf/4QTwf/ANCpof8A4Lof/iaP+EE8H/8AQqaH/wCC6H/4mugooA5//hBPB/8A0Kmh/wDguh/+Jo/4QTwf/wBCpof/AILof/ia6CigDn/+EE8H/wDQqaH/AOC6H/4mj/hBPB//AEKmh/8Aguh/+JroKKAOf/4QTwf/ANCpof8A4Lof/iaP+EE8H/8AQqaH/wCC6H/4mugooA5//hBPB/8A0Kmh/wDguh/+Jo/4QTwf/wBCpof/AILof/ia6CigDn/+EE8H/wDQqaH/AOC6H/4mj/hBPB//AEKmh/8Aguh/+JroKKAOf/4QTwf/ANCpof8A4Lof/iaP+EE8H/8AQqaH/wCC6H/4mugooA5//hBPB/8A0Kmh/wDguh/+Jo/4QTwf/wBCpof/AILof/ia6CigDn/+EE8H/wDQqaH/AOC6H/4mj/hBPB//AEKmh/8Aguh/+JroKKAOf/4QTwf/ANCpof8A4Lof/iaP+EE8H/8AQqaH/wCC6H/4mugooA5//hBPB/8A0Kmh/wDguh/+Jo/4QTwf/wBCpof/AILof/ia6CigDn/+EE8H/wDQqaH/AOC6H/4mj/hBPB//AEKmh/8Aguh/+JroKKAOf/4QTwf/ANCpof8A4Lof/iaP+EE8H/8AQqaH/wCC6H/4mugooA5//hBPB/8A0Kmh/wDguh/+Jr488aQQ2vjrxDb28UcMEWp3KRxxqFVFErAAAcAAcYr7jr4g8d/8lD8S/wDYVuv/AEa1AH1/4E/5J54a/wCwVa/+ilroK5/wJ/yTzw1/2CrX/wBFLXQUAFFFFABRRRQAUUyaaK3heaeVIokG5ndgqqPUk9K8Z+JPxv0+w0+TTvCd7Fd6g5KPcoCUhHqp6MfQjIoA9B8UfEPwz4QPl6tqKrcEZFvEN8hH0HT8ax9D+M/gvXbpbaPUJLWVjhRdx+WGPsckV8r6Zo+t+LdVlWytLzUbpyZJmjQuwyfvMf8AE11n/Ck/HbQNMmjMEAyFkniVz/wEOaAPrsEMoZSCCMgjvS18seAPHfjLwbr0Xhy5sri7iLbP7OuCI3U/7DN0+nSvbf8AhYOqQf8AH74A8TJ6/Zoo5/5PQB0OueK9E8OKG1bUIrbPQOeSO5x1IHt6iodE8Z6D4iuWt9Mv0mmVSWUdsdR9cYP0r51+N/iBPEGr2Ey6VqmnlIiCmpWzQyHn+EHjaPbuaz/gxrOn6J41S51O7s7aBl277ksNp9QchR9Wz7UAfXNNeRI13O6qvTLHArPs/EOiajj7DrGn3WenkXKPn8jXE/HO5ubT4Z3MtrIY3+0RKzADIUkg4Pbr1FAHTah430LTbl7aa7R5kcI6QursmeAWUHIGeORk9s10KsHUMOhGRxivhrw/dr/wkeltekyW8MqjYcYC56c8V9w2zK9rCyjClFIHHAx7UAS0yaaK3iaWaRY41GWZzgD8ax/FHinTfCekSahqMoVFHyqOrH0FfMPin4g+JviVrKaVY+altM+yGyhON+Txu9aAPeNU+NXgjSrs2zak9zIpwxtoiyr/AMCOAfwrpfDni7Q/Flq0+j38Vxt+/GDh0+q9RXi+lfs3vLpgl1PWjFeOuRFFFlUPoSTz+lecatpfiP4S+M4zHOYriI74J0HyTJ9O47EUAfZdFcn8P/HFl468Ox30GI7uPCXUGeY3/wAD1FdZQAUUUUAFFFFABRRRQAV8QeO/+Sh+Jf8AsK3X/o1q+36+IPHf/JQ/Ev8A2Fbr/wBGtQB9f+BP+SeeGv8AsFWv/opa6Cuf8Cf8k88Nf9gq1/8ARS10FABRRVLVtWsdC0u41LUrlLe0gXdJI56ew9SegA60AGr6vYaDpVxqep3KW9pbrukkbt7AdyegA615jJ8Yr/xI32PwB4YvNSuSBvubtfLhhJ/vYOPzZfbNcl/xPfj34o/5bad4PsJfxY/yaQj6hAff5ve9I0iw0HS4NN0y2jtrSBdqRoP1PqT1JPJoA8xh+FWveK5kvPiH4lnu1B3Lpli3lwJ7E4H6DP8AtGvn7x9p9vpXjzWdPtLZbe2trlooolzhUHA68nIwc+9faWo6ja6TptzqF9MsNrbRmSWRuiqBz/8Aqr5c8X+BPEHiHSP+FgRW8s39qSyXEtqEO+CPcRGcdxsC80AaHwB8W6T4f1fU7DVJktzfiLyJn4UFd2VJ7Z3D8q+nEdZEDowZWGQQcgivgJlZGKsCrA4IPBFeh/D/AOLmteDLiO2uHa/0gnD28jEtGPVD2Pt0oA+pdS8NaRq1/Z393ZRPeWcqywzhQHUqcgZ649q1qyvDviPS/FOkRalpNys0DjnsyH0YdjWrQB4J+0XZK8dhcxANIi7pyRkoudseCegJaTOOpC56CvNPhSbZvGEMV1GswfASCRQ0bHIy0gI5VV3EDjLbO2a9t/aC07zvh99viXEsFzGsjjvESflPtv2H8K+bfD+sSaHq8d5GXA2vE5ThgrKVJX/aGcj3AoA+wbn4d+CdTjEj+G9KZXGQ8MCx7ge+UxmuD+Jfwv8ADuk/D3Vr3Rre6tZrdEkWJbyVoiA65yjMR0zXT6P8Z/A+rwqx1f7HKRzFdxmMj8eV/Imsfxv8VfB974a1XR4dRWea6s5YwyAMgJQ45zyc4wB39qAPmHTZJYdTtZIJWilWVSrqQCpz2J4/Pivq3R7P4hyaPa3dh4n0u7ikjBSLUNK8kqOwPltx+FfKFldTWN9BdW+DLE4ZAyhgSPUHr9K+zfh5Kk3hG3aNGCqfKV3zmQIAgYg9GwoDDj5g3FAHzz8YtX8UXWrQ2niBbFPLBRfsJfy2KscnD/MO3X0Uj1MfwYupNO8Ty6hF4evtXkhjJUWW0vFnjJBIBBz7cgY710v7QmgSWd9Z6sqF4JgIDK/LK4aR8Z9CrAf9sxXIfB7xdD4T8apJduEs7uM28rHjbkhg35rj8TQB9C/8LKdOJ/BHjGP1K6YHH/jrmvOvjH4h0jxZ4RXZpWs2moWcolje702SMbejKWxgDnP4V7rZX9rqFrHcWs8csUiB1KMDwRn+tcL8W/FOlaZ4E1SxluonubyBoI4lYFskYJx7fzoA+dPhn40k8E+L7a9d2+wTHyrxBzmM/wAWPUHn8K+m0+LHgSQKR4lsxu6bgw/PI4r4zr66+Et1ZeJ/h1pdzd2ltPc2mbZnkiViGTockddpFAFy9+L/AIFsrWSYa9BOyDIihVmZj6DiuAb9pS1F2VXw9KbcHhvtADEfTHH+elWP2jLc2/hfSPs0CR25u2EvloBzt+XOP+BV4P4W0iHXvFGm6TPcfZ4rudYjJ6Z/zj8aAPpSx/aB8FXUQa5N/Zv3SS33fkVJ/pV2P45eB55kigvbyaRzhUjspCSfTGOa5a5/Zs0h1zba9eRN3DxK4/pj9an8PfABNB8Q6fq6eIZHezuEnEYtx821gcE57jj296APWNE1q21/TEv7SO4SFmKgXELRPwcfdbmtGiigAr4g8d/8lD8S/wDYVuv/AEa1fb9fEHjv/kofiX/sK3X/AKNagD6/8Cf8k88Nf9gq1/8ARS10Fc/4E/5J54a/7BVr/wCilroKAI7idba2luHDlIkLsEQsxAGeAOSfYV89ka78evFOCJtO8IWEv0LH+RkI/BAff5voio4LeG2QpBDHEhZnKxqFBYnJPHckkmgCtpOk2OhaXb6bptslvaQLtjjQdPc+pPUk8k1dJAGTwKK8x1/xI3jrxJN4E8PXMkdtGCdZ1GLokQ4aGM/3mPyk9uevNAEpz8UfEG3k+DdLn5/u6ncKf1iQ/gx9e3pAAAwBgDtVfT9PtNK063sLGBILW3QRxRoOFUVZoA5bxD8OfCviYM2oaRB57f8ALeJdkn5jr+NeC/EL4H6j4agl1PQ3k1DTk+aSPH72EeuB94fTpX1HSEBgQQCDwQaAPiPwh4z1fwVqwvdMnIViPOgY/JKB2YficHtX1d4E+IWl+ONOEtsfJu0VTNbseVJLDA9fuE/TFeD/AB08EWnhjxDbalpsSxWmpb2MSjCxyLjOB2B3Z/OuZ+F2tT6N4804xs4iuZFgm2E8ISMtx/dGT+dAH1B8SbBr/wAE3+F8xII3mkg/57KI3yo9+cj3Ar4yjhDXSwl0AL7d5OF69cnoK+6tbtft2h3kCxtIzxEoqvtJYcrg9jkCvhm+Qw6lcIS7lJWBMibWbB6lexPpQB6DN8FvEEulQ6jpRN9C65ZPLMciHZvIKknkcqQD94beuQNTSfgXqtxcvb3Uo3BkxLGAY9jZIbkruXC/wknJwVHUe0fCW7N54FtZWfcx5JMvmEk92PXJ689sZ+bcB3NAHwfremyaNr1/pkufMtLh4TxjlWI9T6epr6b+BupPqfhuaWe5jluAfmAlLOMu7EuD3Z2dvxPbFeH/ABisxZfFbXIwMB5ElHvvjVj+pNenfs4Xk8ljqds0ymCNgVj8wAgnBztxkjk8kj2BycAHrnirwzZeK9Dl029QMpy0bd0faV3D8GP518reI/hL4q0G7dI9OnvINx2SQIWyNzgcDuQmfxHrX2JRQB8aaX4D8fXY8qy0rVI42xkktGnp3IHb9K7DRfgJ4m1K6jl1y5S2g43kyb5Mcf4n8q+m6r3t5BYWU13cSLHDEhdnc4AAoA+FtasP7K13UNO3bxa3MkIb+8FYjP44r6F/ZtuWbwvrFqT8kd4sgHuyAH/0EV8+a7fDU9f1C+UELPcO6g9cEnH6V9Gfs56c9v4Lv75wQt1dkJkdQqgZ+mSR+FAHo/i/wvZ+MPDdzo97wkoDJIBzG46MK+PvFnhDWfA+tmz1GF0IbdBcoDslA6Mrevt1FfbtZmveHtL8TaXJp2r2iXNu/ZuCp9VI5BoA8T8D/tBRRWcVj4sikMkY2i+hXduHq69c+4r1LTvid4M1Qqtv4gsw7dElfyz/AOPYrw3xr8BdY0Zpbzw+51OyGW8rGJox6Yz831H5V5FLFJDK0UqMkinDKwwQfegD76jkSaNZInV0YZVlOQR6g06vnv8AZ18UXs2oX3hy4meS3W3NzDuOdhDKCB9d2fwr6EoAK+IPHf8AyUPxL/2Fbr/0a1fb9fEHjv8A5KH4l/7Ct1/6NagD6/8AAn/JPPDX/YKtf/RS10Fc/wCBP+SeeGv+wVa/+ilroKACiivKvjH471PQP7M8PeG5Ma3qb4ygBdEPyKBnozMeD22mgBfHnjHUta1r/hAvBT7tUmBF/fKfls4/4huHRueT2zgfMeOz8GeDtM8E6DHpmnJub709ww+ed+7H+g7CqngDwPZ+CNBW2jxNqM+JL67PLSyd+TztBJwPx6k11lABXI/EjxZdeC/B8us2dvFPKk0ceyUkDDHB6V11ZPiPw5pvirSH0vVYmltXZXKqxU5ByORQB89yftHeJGPyaXpyD6OT/Ortn+0pqicXugWkn+1DKyEfgc5/SvSo/gj4DjH/ACCXY9i07nH61n6j8APBd4h+zLe2UnYxT7h+IYH9MUAeFfEX4kXnxAu7ZprVLW2tgfLiVt3J6kmrfwb8NXGv+PLSZUb7LZN500mDgY6DPqa9HX9muxF0GbxBOYM8p5A3fnmvWfC3hLSfB+lCw0mDYh5eRuXkPqTQBs3CCS2lRgpVkIIYZBGO4r4X8RKF8SakoOQLhxnJPf35r7sritb+FXhLXZJprjTI0uZW3mVCQd3qQDzx6/zwaAMX4DyNJ8OogYPKVZWAYE4f3x938ufUZ5Pp9c/4O8J2vgzRTpVncTT24laRDNjcAexxgHHriugoA+UPj5Ft+KM5UcyWsLfpj+lb/wCzibgeINVQM4hEILJlsbs9xnGfqCfpXt2q+BvDuuau2parpsN5OUVB5yAgAdO2T+NTaN4S0bw/eyXOk2cVoJE2NFFGgU85znG4fTOPagDcrxrxH8dU8NeIb3SJ9KNw1s+0Or7SfqD0P/1q9lrj9Y+GHhTXtSkv9Q00SXEmNzBiM0AeSXn7SWoHctloVuoxgNLIT+gxXB+K/it4m8W25tby5WG0JyYYBtB+vrX0ra/CnwRaAbfD9q5Ax+9Bf9CatN8N/Br/AHvDmnn/ALZUAfIPhnwxqfivWIdO02B5HdgGfHyoO5Jr7Q8NaDb+GfDtlpFr/q7aMLn+8e5/E1Y0zRdM0aHytNsLe0Q9RDGFz9fWr1AFLV3kj0W/eFykq28hRh1U7Tg14L4I/aDmhMdl4sgMsfAF7AvzD/fXv9RX0MQGUqQCCMEGvOfFvwW8LeJlee3t/wCzb9ufPt87WPumcflg0AdbYeLvDup2P2yz1qxlgxuJE65X6jORXyf8V9W0vWviHqF5pGw2p2pvQcOwHzMPqa6PUv2fPGFrdMlk1jew5+V1m2HHuGA/TNdn4H/Z/isbqPUPFM8dyyEMtlFnZn/abPP0oAX9nzwZdafbXPiW9iaL7UnlWqsMEpnJb6H+le501ESKNY0UKigBVAwAB2p1ABXxB47/AOSh+Jf+wrdf+jWr7fr4g8d/8lD8S/8AYVuv/RrUAfX/AIE/5J54a/7BVr/6KWugr4cg8aeKrW3it7fxLrMMESBI447+VVRQMAABsAAcYqT/AITvxh/0Neuf+DGb/wCKoA+27ieO1tpbiU7Y4kLucZwAMmvCfhfaT/EH4mat4+1GJha2shjso36K2MKP+AJ192Brxr/hO/GH/Q165/4MZv8A4qo4fGfim3DiHxLrMYdi7bL+UbmPUnDcn3oA+46K+IP+E78Yf9DXrn/gxm/+Ko/4Tvxh/wBDXrn/AIMZv/iqAPt+iviD/hO/GH/Q165/4MZv/iqP+E78Yf8AQ165/wCDGb/4qgD7for4g/4Tvxh/0Neuf+DGb/4qj/hO/GH/AENeuf8Agxm/+KoA+36K+IP+E78Yf9DXrn/gxm/+Ko/4Tvxh/wBDXrn/AIMZv/iqAPt+iviD/hO/GH/Q165/4MZv/iqP+E78Yf8AQ165/wCDGb/4qgD7for4g/4Tvxh/0Neuf+DGb/4qj/hO/GH/AENeuf8Agxm/+KoA+36K+IP+E78Yf9DXrn/gxm/+Ko/4Tvxh/wBDXrn/AIMZv/iqAPt+iviD/hO/GH/Q165/4MZv/iqP+E78Yf8AQ165/wCDGb/4qgD7for4g/4Tvxh/0Neuf+DGb/4qj/hO/GH/AENeuf8Agxm/+KoA+36K+IP+E78Yf9DXrn/gxm/+Ko/4Tvxh/wBDXrn/AIMZv/iqAPt+iviD/hO/GH/Q165/4MZv/iqP+E78Yf8AQ165/wCDGb/4qgD7for4g/4Tvxh/0Neuf+DGb/4qj/hO/GH/AENeuf8Agxm/+KoA+36K+IP+E78Yf9DXrn/gxm/+Ko/4Tvxh/wBDXrn/AIMZv/iqAPt+viDx3/yUPxL/ANhW6/8ARrUf8J34w/6GvXP/AAYzf/FVhzzzXVxLcXEsk08rl5JJGLM7E5JJPJJPOaAP";
		generateImage(base64str,"/Users/zhuweifeng/Downloads/1111/xxx.jpg");
	}
}
