package com.xf.springBootTools.common.util.LetGo;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class GenPojo {
	private String errorNo;
	private String errorMsg;
	private String timestamp;
	private GenData data;

	public String getErrorNo() {
		return errorNo;
	}

	public void setErrorNo(String errorNo) {
		this.errorNo = errorNo;
	}

	public String getErrorMsg() {
		return errorMsg;
	}

	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

	public GenData getData() {
		return data;
	}

	public void setData(GenData data) {
		this.data = data;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "GenPojo{" +
				"errorNo='" + errorNo + '\'' +
				", errorMsg='" + errorMsg + '\'' +
				", timestamp='" + timestamp + '\'' +
				", data=" + data +
				'}';
	}
}
