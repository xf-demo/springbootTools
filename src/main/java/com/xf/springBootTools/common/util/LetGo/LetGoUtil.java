package com.xf.springBootTools.common.util.LetGo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.xf.springBootTools.common.util.Base64Util;
import com.xf.springBootTools.common.util.OkHttpUtil;
import okhttp3.*;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.util.List;

/**
 * OkHttpUtil工具类
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class LetGoUtil {
	/**
	 * 获取小狗列表URL
	 */
	private final static String LIST_URL = "https://pet-chain.baidu.com/data/market/queryPetsOnSale";
	/**
	 * 获取验证码URL
	 */
	private final static String GEN_URL = "https://pet-chain.baidu.com/data/captcha/gen";
	/**
	 * 获取验证码JSON
	 */
	private final static String GEN_JSON = "{\"requestId\":1517987800170,\"appId\":1,\"tpl\":\"\"}";
	/**
	 * 验证码图片保存地址
	 */
	private final static String PIC_PATH = "/Users/zhuweifeng/Downloads/1111/";
	/**
	 * 订单提交地址
	 */
	private final static String BUY_URL = "https://pet-chain.baidu.com/data/txn/create";

	/**
	 * 获取小狗列表JSON
	 */
	private final static String JSON_BODY = "{\n" +
			"    \"pageNo\":1,\n" +
			"    \"pageSize\":10,\n" +
			"    \"querySortType\":\"AMOUNT_ASC\",\n" +
			"    \"petIds\":[\n" +
			"\n" +
			"    ],\n" +
			"    \"lastAmount\":null,\n" +
			"    \"lastRareDegree\":null,\n" +
			"    \"requestId\":1517883128932,\n" +
			"    \"appId\":1,\n" +
			"    \"tpl\":\"\"\n" +
			"}";

	public static void main(String[] args) {
		while (true) {
			try {
				//1.查询pet列表
				PetOnSalePojo pet = findPetList();
				if (pet.getAmount() > 1000) {
					continue;
				}
				System.out.println("发现一只便宜狗:" + pet.getAmount());
				//2.获取验证码
				GenPojo gen = getGen();
				//3.生成本地图片
				String path = createImage(gen.getData().getImg());
				//4.将本地图片上传至解析服务解析
				Long begin = System.currentTimeMillis();
				String code = getValidCode(path);
				System.out.println("验证码获取时间" + (System.currentTimeMillis() - begin) + " ms");
				//5.购买
				buyPet(pet, gen.getData().getSeed(), code);
			} catch (Exception e) {
				//System.out.println("异常:"+e.getMessage());
			}
		}

	}

	/**
	 * 获取最便宜pet
	 *
	 * @return
	 */
	public static PetOnSalePojo findPetList() throws IOException {
		PetOnSalePojo pet = null;
		String ret = OkHttpUtil.post(LIST_URL, JSON_BODY);
		ObjectMapper objectMapper = new ObjectMapper();
		PetListPojo bean = objectMapper.readValue(ret, PetListPojo.class);
		if (bean != null) {
			List<PetOnSalePojo> pets = bean.getData().getPetsOnSale();
			if (!CollectionUtils.isEmpty(pets)) {
				pet = pets.get(0);
				Double amount = pet.getAmount();
				//System.out.println("最便宜:" + amount);
			}
		}
		return pet;
	}

	/**
	 * 获取最便宜pet
	 *
	 * @return
	 */
	public static String buyPet(PetOnSalePojo pet, String seed, String code) throws IOException {
		String BUY_JSON = "{\n" +
				"    \"petId\":\"" + pet.getPetId() + "\",\n" +
				"    \"amount\":\"" + pet.getAmount() + "\",\n" +
				"    \"seed\":\"" + seed + "\",\n" +
				"    \"captcha\":\"" + code + "\",\n" +
				"    \"validCode\":\"" + pet.getValidCode() + "\",\n" +
				"    \"requestId\":1517992390381,\n" +
				"    \"appId\":1,\n" +
				"    \"tpl\":\"\"\n" +
				"}";
		String ret = OkHttpUtil.post(BUY_URL, BUY_JSON);
		System.err.println("购买结果:" + ret);
		return null;
	}


	/**
	 * 请求验证码
	 *
	 * @return 返回验证码路径
	 */
	public static GenPojo getGen() {
		try {
			String ret = OkHttpUtil.post(GEN_URL, GEN_JSON);
			System.out.println("获取验证码结果:" + ret);
			ObjectMapper objectMapper = new ObjectMapper();
			GenPojo genPojo = objectMapper.readValue(ret, GenPojo.class);
			return genPojo;
		} catch (IOException e) {
			System.out.println("验证码获取失败:" + e.getMessage());
			return null;
		}
	}


	/**
	 * 将验证码base64生成本地图片
	 *
	 * @return 返回验证码路径
	 */
	public static String createImage(String imgBase64) {
		try {
			String picName = System.currentTimeMillis() + ".jpg";
			Base64Util.generateImage(imgBase64, PIC_PATH + picName);
			System.out.println("本地生成图片:" + PIC_PATH + picName);
			return PIC_PATH + picName;
		} catch (IOException e) {
			System.out.println("验证码获取失败:" + e.getMessage());
			return null;
		}
	}

	/**
	 * 将本地图片上传至解析服务解析
	 *
	 * @param path
	 * @return
	 */
	private static String getValidCode(String path) throws IOException {
		String code = ChaoJiYing.PostPic("zwfmu229", "q1234567890", "894306", "1004", "0", path);
		ObjectMapper objectMapper = new ObjectMapper();
		CodePojo genPojo = objectMapper.readValue(code, CodePojo.class);
		System.out.println("解析服务得到的code：" + genPojo.getPic_str());
		return genPojo.getPic_str();
	}


}
