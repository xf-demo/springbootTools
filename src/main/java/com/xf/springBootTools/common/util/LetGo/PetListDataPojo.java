package com.xf.springBootTools.common.util.LetGo;

import java.util.List;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 *
 * @author xiaofeng
 * @date 2018/2/7
 */
public class PetListDataPojo {
	private List<PetOnSalePojo> petsOnSale;
	private Integer totalCount;
	private boolean hasData;

	public List<PetOnSalePojo> getPetsOnSale() {
		return petsOnSale;
	}

	public void setPetsOnSale(List<PetOnSalePojo> petsOnSale) {
		this.petsOnSale = petsOnSale;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public boolean isHasData() {
		return hasData;
	}

	public void setHasData(boolean hasData) {
		this.hasData = hasData;
	}
}
