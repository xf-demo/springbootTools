package com.xf.springBootTools.common.service.impl;

import com.github.pagehelper.PageHelper;
import com.xf.springBootTools.common.exception.UpdateFailedException;
import com.xf.springBootTools.common.pojo.dto.BaseDTO;
import com.xf.springBootTools.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 基础service实现类
 *
 * @author xiaofeng
 * @date 2017/12/29
 */
@Transactional(rollbackFor = Exception.class)
public class BaseServiceImpl<T> implements BaseService<T> {
	/**
	 * 注意：在SpringBootApplication入口使用注解tk.mybatis.spring.annotation.MapperScan扫描</br>
	 * 否则会无法注入
	 */
	@Autowired
	protected Mapper<T> mapper;

	/**
	 * 根据条件查询满足条件的记录
	 *
	 * @param condition
	 * @param pageNum
	 * @param pageSize
	 * @return List
	 */
	@Override
	public List<T> select(T condition, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		return mapper.select(condition);
	}

	/**
	 * 保存一个实体，null的属性也会保存，不会使用数据库默认值
	 *
	 * @param record
	 * @return record
	 */
	@Override
	public T insert(T record) {
		mapper.insert(record);
		return record;
	}

	/**
	 * 保存一个实体，null的属性不会保存，会使用数据库默认值
	 *
	 * @param record
	 * @return record
	 */
	@Override
	public T insertSelective(T record) {
		mapper.insertSelective(record);
		return record;
	}

	/**
	 * 根据主键更新实体全部字段，null值会被更新<br>
	 * 通俗说就是如果dto中有值是null也会更新到数据库
	 *
	 * @param record
	 * @return record
	 */
	@Override
	public T updateByPrimaryKey(T record) {
		int ret = mapper.updateByPrimaryKey(record);
		this.checkObjectVersionNumber(ret, record);
		return record;
	}

	/**
	 * 根据主键更新属性不为null的值
	 * 通俗说就是如果dto中有值是null，不会更新到数据库
	 *
	 * @param record
	 * @return
	 */
	@Override
	public T updateByPrimaryKeySelective(T record) {
		int ret = mapper.updateByPrimaryKeySelective(record);
		checkObjectVersionNumber(ret, record);
		return record;
	}

	@Override
	public T selectByPrimaryKey(T record) {
		return mapper.selectByPrimaryKey(record);
	}

	@Override
	public int deleteByPrimaryKey(Object record) {
		return mapper.deleteByPrimaryKey(record);
	}

	@Override
	public List selectAll() {
		return mapper.selectAll();
	}

	@Override
	public List batchUpdate(List list) {
		//TODO
		return null;
	}

	@Override
	public int batchDelete(List list) {
		//TODO
		return 0;
	}

	/**
	 * 检查乐观锁<br>
	 * 检测到更新，删除失败时，抛出UpdateFailedException 异常
	 *
	 * @param updateCount update,delete 操作返回的值
	 * @param record      操作参数
	 */
	protected void checkObjectVersionNumber(int updateCount, Object record) {
		if (updateCount == 0 && record instanceof BaseDTO) {
			BaseDTO baseDTO = (BaseDTO) record;
			if (baseDTO.getObjectVersionNumber() != null) {
				throw new RuntimeException(new UpdateFailedException(baseDTO));
			}
		}
	}
}
