package com.xf.springBootTools.common.service;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author zhuweifeng
 * @date 2017/12/29
 */
public interface BaseService<T> {
	/**
	 * 分页查询
	 * @param condition
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	List<T> select(T condition, int pageNum, int pageSize);

	/**
	 * 插入操作
	 * @param record
	 * @return
	 */
	T insert(T record);

	/**
	 * 插入操作
	 * @param record
	 * @return
	 */
	T insertSelective(T record);

	/**
	 * 更新操作
	 * @param record
	 * @return
	 */
	T updateByPrimaryKey(T record);

	/**
	 * 更新操作
	 * @param record
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	T updateByPrimaryKeySelective(T record);

	/**
	 * 根据主线查询
	 * @param record
	 * @return
	 */
	T selectByPrimaryKey(T record);

	/**
	 * 根据主键查询
	 * @param record
	 * @return
	 */
	int deleteByPrimaryKey(T record);

	/**
	 * 查询所有记录
	 * @return
	 */
	List<T> selectAll();

	/**
	 * 批量更新
	 * @param list
	 * @return
	 */
	List<T> batchUpdate(List<T> list);

	/**
	 * 批量删除
	 * @param list
	 * @return
	 */
	int batchDelete(List<T> list);
}
