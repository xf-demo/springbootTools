package com.xf.springBootTools.common.pojo.security;

import com.xf.springBootTools.common.pojo.dto.Role;
import com.xf.springBootTools.common.pojo.dto.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * @author zhuweifeng
 * @date 2017/12/25
 * SecurityUser
 */
public class SecurityUser implements UserDetails {
	private String username;
	private String name;
	private String password;
	private String email;
	private Long id;
	private Set<Role> roles = new HashSet<Role>(0);
	private Collection<? extends GrantedAuthority> authorities;

	private static final long serialVersionUID = 1L;

	public SecurityUser(User user,Collection<SimpleGrantedAuthority> authorities) {
		if (user != null) {
			this.setId(user.getId());
			this.setUsername(user.getLoginName());
			this.setName(user.getUserName());
			this.setEmail(user.getEmail());
			this.setPassword(user.getPassword());
			this.setRoles(user.getRoles());
			this.authorities = authorities;
		}
	}



	/**
	 * 将用户权限放入集合
	 *
	 * @return
	 */
	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		Set<Role> userRoles = this.getRoles();
		if (userRoles != null) {
			for (Role role : userRoles) {
				SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.getRoleName());
				authorities.add(authority);
			}
		}
		return authorities;
	}


	@Override
	public boolean isAccountNonExpired() {
		return true;

	}


	@Override
	public boolean isAccountNonLocked() {
		return true;
	}


	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}


	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}
}