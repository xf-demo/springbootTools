package com.xf.springBootTools.common.pojo.dto;

/**
 * @author zhuweifeng
 * @date 2017/10/30
 */
public class Org {
	private String id;
	private String orgName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}
}
