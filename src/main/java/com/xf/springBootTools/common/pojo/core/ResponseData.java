package com.xf.springBootTools.common.pojo.core;

import com.github.pagehelper.Page;

import java.util.List;

/**
 * RESTFUL接口返回公共包装类
 *
 * @author xiaofeng
 * @date 2017/11/2
 */
public class ResponseData {
	/**
	 * 是否成功
	 */
	private Boolean success;

	/**
	 * 消息
	 */
	private String msg;
	/**
	 * 数据数量
	 */
	private long count;
	/**
	 * 数据
	 */
	private List<?> data;

	/**
	 * layUI默认返回0
	 */
	private Integer code = 0;

	public ResponseData() {
		this.success = true;
	}

	public ResponseData(List<?> data) {
		this.success = true;
		if(data instanceof Page){
			this.count = ((Page) data).getTotal();
			this.data = ((Page) data).getResult();
		}else {
			this.data = data;
		}
	}

	public ResponseData(long count, List<?> data) {
		this.count = count;
		this.success = true;
		this.data = data;
	}

	public ResponseData(Boolean success, String msg) {
		this.success = success;
		this.msg = msg;
	}

	public ResponseData(Boolean success, String msg, long count, List<?> data, Integer code) {
		this.success = success;
		this.msg = msg;
		this.count = count;
		this.data = data;
		this.code = code;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public List<?> getData() {
		return data;
	}

	public void setData(List<?> data) {
		this.data = data;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}
}
