package com.xf.springBootTools.common.pojo.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Version;
import java.io.Serializable;
import java.util.Date;


/**
 * 标注DTO类
 *
 * @author xiaofeng
 * @date 2017/11/2
 */
public class BaseDTO implements Serializable {
	/**
	 * Record的版本号，每发生update则自增.
	 * <p>
	 * 用于实现乐观锁,无法替代数据库锁.
	 * <p>
	 * 不能通过 update xx set object_version_number = 3 where xxx 来更新.<br>
	 * 只能是自动的 update xx set a=1, object_version_number = object_version_number +1  where xxx
	 */
	@Column
	@Version
	private Long objectVersionNumber;

	/**
	 * 创建人
	 */
	@Column(updatable = false)
	private Long createdBy;

	/**
	 * 创建时间
	 */
	@Column
	private Date creationDate;

	/**
	 * 最后更新人
	 */
	@Column
	private Long lastUpdatedBy;

	/**
	 * 最后更新时间
	 */
	@Column
	private Date lastUpdateDate;

	public Long getObjectVersionNumber() {
		return objectVersionNumber;
	}

	public void setObjectVersionNumber(Long objectVersionNumber) {
		this.objectVersionNumber = objectVersionNumber;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(Long lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Date getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	@Override
	public String toString() {
		return "BaseDTO{" +
				"objectVersionNumber=" + objectVersionNumber +
				", createdBy=" + createdBy +
				", creationDate=" + creationDate +
				", lastUpdatedBy=" + lastUpdatedBy +
				", lastUpdateDate=" + lastUpdateDate +
				'}';
	}
}