package com.xf.springBootTools.config;

import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author xiaofeng
 * @date 2017/12/25
 * 将spring-session配置注入security
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {

        public SecurityInitializer() {
                super(SecurityConfig.class, RedisConfig.class);
        }
}