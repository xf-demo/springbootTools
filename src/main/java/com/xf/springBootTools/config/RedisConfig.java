package com.xf.springBootTools.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.*;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * @author xiaofeng
 * @date 2017/12/25
 * redis配置
 */
@EnableRedisHttpSession
public class RedisConfig {
        @Value("${spring.redis.host:localhost}")
        private String host;
        @Value("${spring.redis.port:6379}")
        private int port;
        @Value("${spring.redis.password}")
        private String password;
        @Value("${spring.redis.database}:0")
        private int database;

        @Bean
        public JedisConnectionFactory jedisConnectionFactory() {
                JedisConnectionFactory connection = new JedisConnectionFactory();
                connection.setPort(port);
                connection.setHostName(host);
                connection.setPassword(password);
                connection.setDatabase(database);
                return connection;
        }
}