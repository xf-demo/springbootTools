package com.xf.springBootTools.config;

import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
 * @author zhuweifeng
 * @date 2017/12/25
 * 确保springSessionRepositoryFilter在springSecurityFilterChain前调用
 * By extending AbstractHttpSessionApplicationInitializer we ensure that the Spring Bean by the name springSessionRepositoryFilter is registered with our Servlet Container for every request before Spring Security’s springSecurityFilterChain .
 */
public class SpringSessionInitializer extends AbstractHttpSessionApplicationInitializer {

}