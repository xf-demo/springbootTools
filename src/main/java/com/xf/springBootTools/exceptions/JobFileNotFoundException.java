package com.xf.springBootTools.exceptions;

/**
 * 任务类未找到异常</br>
 * 自定义异常类(继承运行时异常)
 *
 * @author xiaofeng
 * @version 2016/11/26
 */
public class JobFileNotFoundException extends RuntimeException {
	public static final String JOB_FILE_NOT_FOUND="任务类未找到";

	private static final long serialVersionUID = 1L;

	/**
	 * 错误编码
	 */
	private String errorCode;

	/**
	 * 构造一个基本异常.
	 *
	 * @param message 信息描述
	 */
	public JobFileNotFoundException(String message) {
		super(message);
	}

	/**
	 * 构造一个基本异常.
	 *
	 * @param errorCode 错误编码
	 * @param message   信息描述
	 */
	public JobFileNotFoundException(String errorCode, String message) {
		super(message);
		this.setErrorCode(errorCode);
	}

	/**
	 * 构造一个基本异常.
	 *
	 * @param errorCode 错误编码
	 * @param message   信息描述
     * @param message   异常
	 */
	public JobFileNotFoundException(String errorCode, String message, Throwable cause) {
		super(message, cause);
		this.setErrorCode(errorCode);
	}

	/**
	 * 构造一个基本异常.
	 *
	 * @param message 信息描述
	 * @param cause   根异常类（可以存入任何异常）
	 */
	public JobFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


}