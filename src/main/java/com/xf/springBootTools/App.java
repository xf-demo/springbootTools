package com.xf.springBootTools;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author zhuweifeng
 * @date 2017/10/30
 * 注意此处使用的通用mapper的扫描注解tk.mybatis.spring.annotation.MapperScan
 */
@SpringBootApplication
@MapperScan(basePackages = "com.xf.springBootTools.modules.*.mapper")
@ComponentScan("com.xf.springBootTools")
public class App {
	public static void main(String[] args) {
		SpringApplication.run(App.class,args);
	}
}
