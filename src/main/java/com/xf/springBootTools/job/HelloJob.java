package com.xf.springBootTools.job;

import org.assertj.core.api.Assert;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

/**
 * job测试类
 */
@Component
public class HelloJob extends AbstractJob {

    @Override
    public void executeJob(JobExecutionContext context) throws Exception {
        System.out.println("hello");
    }

}
