package com.xf.springBootTools.job;

import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

/**
 * job测试类
 */
@Component
public class HelloJob2 extends AbstractJob {

    @Override
    public void executeJob(JobExecutionContext context) throws Exception {
        System.out.println("hello2");
        throw new Exception("hello exception");
    }

}
