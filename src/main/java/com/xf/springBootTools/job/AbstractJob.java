package com.xf.springBootTools.job;

import com.xf.springBootTools.config.SpringContextUtils;
import com.xf.springBootTools.modules.quartz.dto.QrtzJobHistory;
import com.xf.springBootTools.modules.quartz.dto.QrtzTriggers;
import com.xf.springBootTools.modules.quartz.dto.QrtzTriggersPk;
import com.xf.springBootTools.modules.quartz.mapper.repository.JobHistoryRepository;
import com.xf.springBootTools.modules.quartz.mapper.repository.JobTriggersRepository;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

/**
 * @author xiaofeng
 * job抽象类，用于处理job的公共方法 </br>
 * JobListener 如果不需要的话可以自行去除
 */
public abstract class AbstractJob implements Job, JobListener {
	private static Logger log = LoggerFactory.getLogger(AbstractJob.class);

	private JobHistoryRepository jobHistoryRepository;
	private JobTriggersRepository jobDetailRepository;


	/**
	 * 自动处理所有Job运行时发生的异常.
	 * <p>
	 * <p>
	 *
	 * @param context Job运行时的上下文。
	 * @throws JobExecutionException if there is an exception while executing the job.
	 */
	@Override
	public final void execute(JobExecutionContext context) throws JobExecutionException {
		String success = "SUCCESS";
		String remark = null;
		try {
			// check job is running or not
			log.info("check job is running?");
			if(!checkJobStatus(context)){
				log.info("has job is running");
				log.info("有相同job正在运行中，下次再运行");
				return;
			}
			log.info("no job is running,do job");
			// do job
			executeJob(context);
			log.info("job exceute success");
		} catch (Exception e) {
			log.error("job execute failed:",e);
			success = "ERROR";
			remark = e.getMessage();
		}finally {
			// save job history
			saveJobHistory(context,success,remark);
		}
	}

	/**
	 * save job history
	 * @param context
	 */
	private boolean checkJobStatus(JobExecutionContext context){
		String WAITING_STATE = "WAITING";
		jobDetailRepository =  SpringContextUtils.getBean(JobTriggersRepository.class);
		TriggerKey triggerKey = context.getTrigger().getKey();
		String schedulerName = "schedulerFactory";
		try {
			schedulerName = context.getScheduler().getSchedulerName();
		}catch (Exception e){
			log.error("",e);
		}
		QrtzTriggersPk qrtzTriggersPk = new QrtzTriggersPk(schedulerName,triggerKey.getName(),triggerKey.getGroup());
		QrtzTriggers qrtzTriggers = jobDetailRepository.findQrtzTriggersById(qrtzTriggersPk);
		String triggerState = qrtzTriggers.getTriggerState();
		if(StringUtils.equals(triggerState,WAITING_STATE)){
			return true;
		}else {
			return false;
		}
	}

	/**
	 * save job history
	 * @param context
	 * @param success
	 */
	private void saveJobHistory(JobExecutionContext context, String success, String remark){
		QrtzJobHistory jobStatus = new QrtzJobHistory();
		JobKey jobkey = context.getJobDetail().getKey();
		jobStatus.setJobGroup(jobkey.getGroup());
		jobStatus.setJobName(jobkey.getName());
		jobStatus.setJobClassName(context.getJobDetail().getJobClass().getName());
		jobStatus.setCreateTime(new Date());
		jobStatus.setStatus(success);
		jobStatus.setRemark(remark);
		try {
			jobStatus.setHostName(InetAddress.getLocalHost().toString());
		} catch (UnknownHostException e) {
			log.error("",e);
		}
		jobHistoryRepository = SpringContextUtils.getBean(JobHistoryRepository.class);
		jobHistoryRepository.save(jobStatus);
		log.info("save job history success");
	}

	/**
	 * 运行时所有的异常将会被自动处理.
	 *
	 * @param context Job运行时的上下文。
	 * @throws Exception 运行时异常或实现子类主动抛出的异常
	 */
	public abstract void executeJob(JobExecutionContext context) throws Exception;

	/**
	 * 发生异常时是否立即重新执行JOB或将JOB挂起.
	 * <p>
	 *
	 * @return {@code true} Job运行产生异常时，立即重新执行JOB. <br>
	 * {@code false} Job运行产生异常时，挂起JOB等候管理员处理.
	 */
	protected boolean isRefireImmediatelyWhenException() {
		return false;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public void jobToBeExecuted(JobExecutionContext jobExecutionContext) {
	}

	@Override
	public void jobExecutionVetoed(JobExecutionContext jobExecutionContext) {
	}

	@Override
	public void jobWasExecuted(JobExecutionContext jobExecutionContext, JobExecutionException e) {
	}
}

