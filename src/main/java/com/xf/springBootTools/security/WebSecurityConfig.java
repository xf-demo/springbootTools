package com.xf.springBootTools.security;

import com.xf.springBootTools.common.util.Md5Util;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * springSecurity配置
 *
 * @author zhuweifeng
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * WebSecurityConfigurerAdapter，并重写它的方法来设置一些web安全的细节configure(HttpSecurity http)方法
	 *
	 * @param http
	 * @throws Exception
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				//配置不需要验证的URI
				.authorizeRequests()
				// 所有用户均可访问的资源
				.antMatchers("/css/**", "/js/**", "/datas/**", "/images/**", "/jquery/**", "/plugins/**").permitAll()
				// 任何尚未匹配的URL只需要验证用户即可访问
				.anyRequest().authenticated()
				.and().formLogin().loginPage("/login").failureUrl("/login?error=-1").defaultSuccessUrl("/index").permitAll()
				.and().logout().logoutUrl("/logout").logoutSuccessUrl("/login")
				//设置页面可以被iframe
				.and().headers().frameOptions().disable()
				.and().csrf().disable();
	}

	/**
	 * 用户认证
	 *
	 * @param auth
	 * @throws Exception
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		//user Details Service验证
		auth.userDetailsService(customUserDetailsService())
				.passwordEncoder(passwordEncoderHasSalt());
	}


	/**
	 * MD5加密(无盐)
	 *
	 * @return
	 */
	@Bean
	public Md5PasswordEncoder passwordEncoderNoSalt() {
		return new Md5PasswordEncoder();
	}

	/**
	 * MD5加密(有盐)
	 *
	 * @return
	 */
	@Bean
	public PasswordEncoder passwordEncoderHasSalt() {
		return new PasswordEncoder() {
			@Override
			public String encode(CharSequence rawPassword) {
				return Md5Util.encode((String) rawPassword);
			}

			@Override
			public boolean matches(CharSequence rawPassword, String encodedPassword) {
				return encodedPassword.equals(Md5Util.encode((String) rawPassword));
			}
		};
	}

	/**
	 * 自定义UserDetailsService，从数据库中读取用户信息
	 *
	 * @return
	 */
	@Bean
	public CustomUserDetailsService customUserDetailsService() {
		return new CustomUserDetailsService();
	}

	public static void main(String[] args) {
		Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
		System.out.println(passwordEncoder.encodePassword("admin", null));
	}
}