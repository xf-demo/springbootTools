package com.xf.springBootTools.security;

import com.xf.springBootTools.common.pojo.dto.User;
import com.xf.springBootTools.common.pojo.security.SecurityUser;
import com.xf.springBootTools.modules.core.dao.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.Collection;

/**
 * 自定义CustomUserDetailsService实现UserDetailsService
 *
 * @author zhuweifeng
 * @date 2017/12/25
 */
public class CustomUserDetailsService implements UserDetailsService {
	@Autowired
	private UserRepository userRepository;

	/**
	 * 重写根据用户名查询用户方法
	 *
	 * @param loginName
	 * @return
	 * @throws UsernameNotFoundException
	 */
	@Override
	public UserDetails loadUserByUsername(String loginName) throws UsernameNotFoundException {
		User user = userRepository.findUserByLoginName(loginName);
		if (user == null) {
			throw new UsernameNotFoundException("loginName " + loginName + " not found");
		}
		Collection<SimpleGrantedAuthority> authorities = new ArrayList<SimpleGrantedAuthority>();
		//模拟角色
		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		SecurityUser securityUser = new SecurityUser(user, authorities);
		return securityUser;
	}
}
