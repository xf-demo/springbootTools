package com.xf.springBootTools.queue;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 队列监听示例
 * 暂时关闭，如果rabbitmq服务没开，会影响应用启动速度
 * @author zhuweifeng
 * @date 2017/11/2
 */
//@Component
//@RabbitListener(queues = "hello")
public class TestQueue {
	private int count = 0;

	//@RabbitHandler
	public void receive(Object msg) throws Exception {
		Message message = (Message) msg;
		byte[] bytes = message.getBody();
		String body = new String(bytes);
		System.out.println(body);
	}
}
