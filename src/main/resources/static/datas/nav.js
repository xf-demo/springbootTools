var navs = [{
	"title": "系统管理",
	"icon": "fa-cubes",
	"spread": true,
	"children": [{
        "title": "计划任务",
        "icon": "&#xe60a;",
        "href": "/job/index"
    },{
        "title": "代码生成器",
        "icon": "&#xe60a;",
        "href": "/generator/index"
    }]
},{
    "title": "用户组织管理",
    "icon": "fa-cubes",
    "spread": true,
    "children": [{
        "title": "组织管理",
        "icon": "&#xe60a;",
        "href": "/org/orgList"
    },{
        "title": "用户管理",
        "icon": "&#xe60a;",
        "href": "/user/index"
    }]
}];