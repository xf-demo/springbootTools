package ${package}.service;

<#list import as e>
import ${e};
</#list>
import ${package}.dto.${dtoName};
import ${projectPackage}.common.service.BaseService;

public interface ${serviceName} extends BaseService<${dtoName}>{

}