package ${package}.controllers;

<#list import as e>
import ${e};
</#list>
import ${package}.dto.${dtoName};
import ${package}.service.${serviceName};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;
import org.springframework.validation.BindingResult;
import java.util.List;

    @Controller
    public class ${controllerName} {

    @Autowired
    private ${serviceName} ${serviceNameAutowired};


    @RequestMapping(value = "${queryUrl}")
    @ResponseBody
    public ResponseData query() {
        return new ResponseData();
    }

    @RequestMapping(value = "${submitUrl}")
    @ResponseBody
    public ResponseData update( HttpServletRequest request,@RequestBody List<${dtoName}> dto){
        return new ResponseData();
    }

    @RequestMapping(value = "${removeUrl}")
    @ResponseBody
    public ResponseData delete(HttpServletRequest request,@RequestBody List<${dtoName}> dto){
        return new ResponseData();
    }
    }