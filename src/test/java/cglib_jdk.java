import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 *
 * @author xiaofeng
 * @date 2018/3/5
 */
public class cglib_jdk {
	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < 100000; i++) {
			sb.append(i + ",");
		}
		String str = sb.toString();
		str = str.substring(0, str.length() - 1);
		List<String> li = splitStr(str, ",", 5);
		System.out.println(li);
	}

	/**
	 * 切割字符串
	 *
	 * @param str    需要切割的字符串
	 * @param split  分割符号
	 * @param perNum 分割后每块个数
	 * @return
	 */
	public static List<String> splitStr(String str, String split, int perNum) {
		List<String> result = new ArrayList<>();
		List<List<String>> splitResult = new ArrayList<>();
		if (StringUtils.isBlank(str)) {
			return Collections.EMPTY_LIST;
		}
		String[] array = str.split(split);
		List<String> list = Arrays.asList(array);
		splitResult = splitList(list, perNum);
		for (List<String> li : splitResult){
			StringBuffer sb = new StringBuffer();
			for(String tempStr : li){
				sb.append(tempStr+",");
			}
			String sbStr = sb.toString();
			sbStr = sbStr.substring(0,sbStr.length()-1);
			result.add(sbStr);
		}
		return result;
	}




	public static <T> List<List<T>> splitList(List<T> list, int pageSize) {
		List<List<T>> listArray = new ArrayList<List<T>>();
		List<T> subList = null;
		for (int i = 0; i < list.size(); i++) {
			//每次到达页大小的边界就重新申请一个subList
			if (i % pageSize == 0) {
				subList = new ArrayList<T>();
				listArray.add(subList);
			}
			subList.add(list.get(i));
		}
		return listArray;
	}
}
