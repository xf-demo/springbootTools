package cglib;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 *
 * @author xiaofeng
 * @date 2018/3/5
 */
public class Book {
	public void read(){
		System.out.println("i'm reading...");
	}

	public Book() {
	}
}
