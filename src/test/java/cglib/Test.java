package cglib;

/**
 * (这里用一句话描述这是一个什么样的类，格式：本类是XXXXXX。)
 * 动态代理
 * @author xiaofeng
 * @date 2018/3/5
 */
public class Test {
	public static void main(String[] args) {
		MyCglib myCglib = new MyCglib();
		Book book = (Book)myCglib.getProxy(new Book());
		book.read();
	}
}
